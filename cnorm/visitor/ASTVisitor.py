from pyrser import parsing, meta
from cnorm import nodes


@meta.add_method(parsing.node.Node)
def visit(self, parent=None, level=0):
    '''
        Visitor for parsing.node.Node
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Expr)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Expr
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Func)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Func
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.call_expr.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)

@meta.add_method(nodes.BlockInit)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.BlockInit
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    for expr in self.body:
        expr.visit(self, level+1)

@meta.add_method(nodes.BlockExpr)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.BlockExpr
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    for expr in self.body:
        expr.visit(self, level+1)

@meta.add_method(nodes.Unary)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Unary
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))


@meta.add_method(nodes.Paren)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Paren
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if not isinstance(self.call_expr, str):
        self.call_expr.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)

@meta.add_method(nodes.Array)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Array
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Dot)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Dot
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Arrow)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Arrow
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Post)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Post
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Sizeof)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Sizeof
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Binary)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Binary
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.call_expr.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)

@meta.add_method(nodes.Cast)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Cast
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.call_expr.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)


@meta.add_method(nodes.Range)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Range
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.call_expr.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)


@meta.add_method(nodes.Ternary)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Ternary
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    for expr in self.call_expr:
        expr.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)


@meta.add_method(nodes.Terminal)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Terminal
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Id)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Id
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Literal)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Literal
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Raw)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Raw
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Enumerator)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Enumerator
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.expr.visit(self, level+1);

@meta.add_method(nodes.DeclType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.DeclType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if self._decltype is not None:
        self._decltype.visit(self, level+1)

@meta.add_method(nodes.PointerType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.PointerType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if self._decltype is not None:
        self._decltype.visit(self, level+1)

@meta.add_method(nodes.ArrayType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.ArrayType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if self._decltype is not None:
        self._decltype.visit(self, level+1)
    self.expr.visit(self, level+1)

@meta.add_method(nodes.ParenType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.ParenType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if self._decltype is not None:
        self._decltype.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)

@meta.add_method(nodes.QualType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.QualType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if self._decltype is not None:
        self._decltype.visit(self, level+1)

@meta.add_method(nodes.AttrType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.AttrType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.CType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.CType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.PrimaryType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.PrimaryType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.ComposedType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.ComposedType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if hasattr(self, 'fields'):
        for field in self.fields:
            field.visit(self, level+1)
    if hasattr(self, 'enums'):
        for enum in self.enums:
            enum.visit(self, level+1)

@meta.add_method(nodes.FuncType)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.FuncType
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    if self._decltype is not None:
        self._decltype.visit(self, level+1)
    for param in self.params:
        param.visit(self, level+1)

@meta.add_method(nodes.Decl)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Decl
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.ctype.visit(self, level+1)
    assign_expr = self.assign_expr()
    colon_expr = self.colon_expr()
    if assign_expr is not None:
        assign_expr.visit(self, level+1)
    if colon_expr is not None:
        colon_expr.visit(self, level+1)
    if hasattr(self, 'body'):
        self.body.visit(self, level+1)

@meta.add_method(nodes.Stmt)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Stmt
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.ExprStmt)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.ExprStmt
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.expr.visit(self, level+1)

@meta.add_method(nodes.BlockStmt)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.BlockStmt
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    for expr in self.body:
        expr.visit(self, level+1)
    
@meta.add_method(nodes.RootBlockStmt)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.RootBlockStmt
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    for expr in self.body:
        expr.visit(self, level+1)

@meta.add_method(nodes.Label)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Label
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Branch)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Branch
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.expr.visit(self, level+1)

@meta.add_method(nodes.Case)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Case
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.expr.visit(self, level+1)

@meta.add_method(nodes.Return)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Return
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.expr.visit(self, level+1)

@meta.add_method(nodes.Goto)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Goto
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.expr.visit(self, level+1)

@meta.add_method(nodes.LoopControl)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.LoopControl
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Break)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Break
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Continue)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Continue
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))

@meta.add_method(nodes.Conditional)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Conditional
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.condition.visit(self, level+1)

@meta.add_method(nodes.If)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.If
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.condition.visit(self, level+1)
    self.thencond.visit(self, level+1)
    if self.elsecond is not None:
        self.elsecond.visit(self, level+1)

@meta.add_method(nodes.While)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.While
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.condition.visit(self, level+1)
    self.body.visit(self, level+1)

@meta.add_method(nodes.Switch)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Switch
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.condition.visit(self, level+1)
    self.body.visit(self, level+1)

@meta.add_method(nodes.Do)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.Do
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.condition.visit(self, level+1)
    self.body.visit(self, level+1)

@meta.add_method(nodes.For)
def visit(self, parent=None, level=0):
    '''
        Visitor for nodes.For
    '''
    spaces = "    " * (level - 1) + "`---" if level > 0 else ""
    print("{spaces}{node}".format(spaces=spaces,
                                  node=type(self).__name__))
    self.init.visit(self, level+1)
    self.condition.visit(self, level+1)
    self.increment.visit(self, level+1)
    self.body.visit(self, level+1)